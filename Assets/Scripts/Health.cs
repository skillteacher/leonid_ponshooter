using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private float startHitPoints = 100f;
    private float hitPoints;

    private void Start()
    {
        hitPoints = startHitPoints;
    }

    public void TakeDamage(float damage)
    {
        hitPoints = hitPoints - damage;
        if(hitPoints > 0f)
        {
            Destroy(gameObject);
        }
    }
}
