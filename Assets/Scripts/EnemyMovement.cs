using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float speed = 10f;
    [SerializeField] private float visionrange = 10f;
    private NavMeshAgent agent;


    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        if (IsTargetInVisionRange())
        {
            agent.SetDestination(target.position);
        }
    }

    private bool IsTargetInVisionRange()
    {
        float distanceToTarget = Vector3.Distance(transform.position, target.position);
        return visionrange > distanceToTarget;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, visionrange);
    }
}
